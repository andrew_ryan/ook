#[allow(warnings)]
pub mod utils {
    use std::collections::HashMap;

    pub fn bf_brackets_balance(bf: &str) -> i32 {
        let mut histogram = HashMap::new();
        for c in bf.chars() {
            *histogram.entry(c).or_insert(0) += 1;
        }
        let open_brackets = *histogram.get(&'[').unwrap_or(&0);
        let close_brackets = *histogram.get(&']').unwrap_or(&0);
        (open_brackets as i32) - (close_brackets as i32)
    }

    pub fn bf_count_chars(bf: &str) -> i32 {
        bf.split(|c: char| !"[.+-><,]".contains(c))
            .filter(|s| !s.is_empty())
            .count() as i32
    }

    pub fn bf_reduce(bf: &str) -> String {
        bf.chars().filter(|c| "[.+-><,]".contains(*c)).collect()
    }
}

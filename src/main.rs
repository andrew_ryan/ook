#[allow(warnings)]
fn main() {
    use ook::Ook::brainfuck_to_text;
    use ook::Ook::text_to_brainfuck;
    use ook::Ook::ook_to_text;
    use ook::Ook::text_to_short_ook;
    use ook::Ook::text_to_ook;
    
    let text = "demo";
    assert_eq!(&text_to_brainfuck(text),"++++++++++[->++++++++++<]>.+.++++++++.++.<");//encode text to brainfuck

    assert_eq!(&brainfuck_to_text(text_to_brainfuck(text)),"demo"); //decode text from brainfuck

    assert_eq!(&text_to_ook(text),"Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook! Ook! Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook. Ook? Ook! Ook. Ook? Ook! Ook. Ook. Ook. Ook! Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook? Ook. ");//encode text to ook

    assert_eq!(text_to_short_ook(text),"....................!?!!.?....................?.?!.?!...!.................!.....!.?.");//encode text to ook short
    
    assert_eq!(ook_to_text(text_to_ook(text)),"demo");//decode text from ook
}

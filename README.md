# ook lib is for Brainfuck/Ook! Obfuscation/Encoding

This tool can run encode an decode text written in the [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck "https://en.wikipedia.org/wiki/Brainfuck") and [Ook!](https://esolangs.org/wiki/Ook! "https://esolangs.org/wiki/Ook!") programming languages and return the output.

[![Crates.io](https://img.shields.io/crates/v/ook.svg)](https://crates.io/crates/ook)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/ook)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/ook/-/raw/master/LICENSE)

```rust
#[allow(warnings)]
fn main() {
    use ook::Ook::brainfuck_to_text;
    use ook::Ook::text_to_brainfuck;
    use ook::Ook::ook_to_text;
    use ook::Ook::text_to_short_ook;
    use ook::Ook::text_to_ook;
    
    let text = "demo";
    assert_eq!(&text_to_brainfuck(text),"++++++++++[->++++++++++<]>.+.++++++++.++.<");//encode text to brainfuck

    assert_eq!(&brainfuck_to_text(text_to_brainfuck(text)),"demo"); //decode text from brainfuck

    assert_eq!(&text_to_ook(text),"Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook! Ook! Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook. Ook? Ook! Ook. Ook? Ook! Ook. Ook. Ook. Ook! Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook? Ook. ");//encode text to ook

    assert_eq!(text_to_short_ook(text),"....................!?!!.?....................?.?!.?!...!.................!.....!.?.");//encode text to ook short
    
    assert_eq!(ook_to_text(text_to_ook(text)),"demo");//decode text from ook
}

```

